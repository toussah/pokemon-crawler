from django.db import models


class Type(models.Model):
    """
    Stores all pokemon types and related attributes.
    See: https://pokeapi.co/docs/v2#types
    """
    _endpoint = 'type'

    id = models.PositiveIntegerField(primary_key=True, help_text='Primary key as found on pokeapi')
    name = models.CharField(max_length=32, unique=True, help_text='Unique name for the type')

    def __str__(self):
        """
        Returns the type's name as a short-hand for its representation.
        """
        return self.name

    class Meta:
        ordering = ['name']


class Ability(models.Model):
    """
    Stores all pokemon types and related attributes.
    See: https://pokeapi.co/docs/v2#ability
    """
    _endpoint = 'ability'

    id = models.PositiveIntegerField(primary_key=True, help_text='Primary key as found on pokeapi')
    name = models.CharField(max_length=64, help_text='Unique name for the type')

    def __str__(self):
        """
        Returns the ability's name as a short-hand for its representation.
        """
        return self.name

    class Meta:
        ordering = ['name']


class Move(models.Model):
    """
    Stores information related to a pokemon's move.
    See: https://pokeapi.co/docs/v2#moves-section
    """
    _endpoint = 'move'

    id = models.PositiveIntegerField(primary_key=True, help_text='Primary key as found on pokeapi')
    name = models.CharField(max_length=64, unique=True, help_text="Ability/move name")
    accuracy = models.PositiveIntegerField(null=True)
    pp = models.PositiveIntegerField(null=True, verbose_name="PP", help_text="Base number of times the move can be used")
    power = models.PositiveIntegerField(null=True)
    priority = models.IntegerField(null=True)
    type = models.ForeignKey(to=Type, null=True, blank=False, related_query_name='move', related_name='moves', on_delete=models.SET_NULL)

    def __str__(self):
        """
        Returns the move's name as a short-hand for its representation.
        """
        return self.name

    class Meta:
        ordering = ['name']


class Pokemon(models.Model):
    """
    Stores information related to each pokemon.
    We use the national pokedex entry number as the primary key.
    See: https://pokeapi.co/docs/v2#pokemon
    """
    _endpoint = 'pokemon'

    id = models.IntegerField(primary_key=True, verbose_name='National Dex No')
    name = models.CharField(unique=True, max_length=128)
    height = models.PositiveIntegerField(null=True)
    weight = models.PositiveIntegerField(null=True)
    moves = models.ManyToManyField(to=Move, blank=True, related_query_name='pokemon', related_name='pokemons')
    types = models.ManyToManyField(to=Type, blank=True, related_query_name='pokemon', related_name='pokemons')
    abilities = models.ManyToManyField(to=Ability, blank=True, related_query_name='pokemon', related_name='pokemons')

    def __str__(self):
        """
        Human-readable representation of a pokemon instance, composed of its national pokedex number and its name.
        """
        return f"#{self.id} - {self.name}"

    class Meta:
        ordering = ['name']
