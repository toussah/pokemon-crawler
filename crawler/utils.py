from crawler.crawler import Crawler
from crawler.serializers import AbilitySerializer, MoveSerializer, PokemonSerializer, TypeSerializer


def refresh_pokemons():
    """
    Refresh all pokemons and related tables from pokeapi.
    The order matters because of the hierarchy of relational keys.
    """
    for serializer in (TypeSerializer, AbilitySerializer, MoveSerializer, PokemonSerializer):
        crawler = Crawler(endpoint=serializer.Meta.model._endpoint, serializer=serializer)
        crawler.save_results()
