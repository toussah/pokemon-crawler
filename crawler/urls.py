from crawler import views
from django.urls import path

urlpatterns = [
    # List views.
    path(r'api/v1/types', views.TypeListAPI.as_view(), name='types'),
    path(r'api/v1/abilities', views.AbilityListAPI.as_view(), name='abilities'),
    path(r'api/v1/moves', views.MoveListAPI.as_view(), name='moves'),
    path(r'api/v1/pokemons', views.PokemonListAPI.as_view(), name='pokemons'),
    # Get (read-only) views.
    path(r'api/v1/type/<str:name>', views.TypeAPI.as_view(), name='type'),
    path(r'api/v1/ability/<str:name>', views.AbilityAPI.as_view(), name='ability'),
    path(r'api/v1/move/<str:name>', views.MoveAPI.as_view(), name='move'),
    path(r'api/v1/pokemon/<str:name>', views.PokemonAPI.as_view(), name='pokemon'),
    path(r'api/v1/pokedex/<int:id>', views.PokedexAPI.as_view(), name='pokedex'),
    # Refresh
    path(r'api/v1/refresh-pokemons', views.RefreshPokemonsAPI.as_view(), name='refresh-pokemons')
]
