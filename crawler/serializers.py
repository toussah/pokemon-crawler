from rest_framework import fields, serializers
from crawler.models import Ability, Move, Pokemon, Type


def validate_fk(values, path):
    """
    Extracts primary key or primary keys from api results.
    """
    if not isinstance(values, (dict, list)):
        return values
    try:
        if isinstance(values, dict):
            # The primary key is the resource ID of the URL.
            return int(values['url'].strip('/').split('/')[-1])
        elif path:
            values = [value[path] for value in values]
        return [int(value['url'].strip('/').split('/')[-1]) for value in values]
    except KeyError:
        return values


class APISerializer(serializers.ModelSerializer):
    """
    Overrides DRF Serializer to parse foreign keys from dictionary and lists sent by the API.
    e.g. {"type": {"url": "https://pokeapi.co/api/v2/type/1/}} to {"type": 2}
    and {"types": [{"url": "https://pokeapi.co/api/v2/type/1/}} to {"type": [2]}
    """
    def __init__(self, instance=None, data=fields.empty, **kwargs):
        """
        Preprocesses the data sent
        """
        if isinstance(data, dict):
            fk_map = getattr(self, '_fk_map', [])
            for field, path in fk_map:
                if field in data:
                    data[field] = validate_fk(data[field], path)
        return super().__init__(instance, data, **kwargs)

    def to_representation(self, value):
        """
        Display name only for representation.
        """
        return value.name


class TypeSerializer(APISerializer):

    class Meta:
        model = Type
        fields = '__all__'


class AbilitySerializer(APISerializer):

    class Meta:
        model = Ability
        fields = '__all__'


class MoveSerializer(APISerializer):

    _fk_map = (('type', None), )

    class Meta:
        model = Move
        fields = '__all__'


class PokemonSerializer(APISerializer):

    _fk_map = (('moves', 'move'), ('types', 'type'), ('abilities', 'ability'))

    class Meta:
        model = Pokemon
        fields = '__all__'


##################################################
#
# Read-only serializers
#
class MoveReadOnlySerializer(serializers.ModelSerializer):

    type = serializers.CharField(read_only=True, source='type.name')

    class Meta:
        model = Move
        fields = '__all__'


class PokemonReadOnlySerializer(serializers.ModelSerializer):

    abilities = AbilitySerializer(many=True, read_only=True)
    moves = MoveSerializer(many=True, read_only=True)
    types = TypeSerializer(many=True, read_only=True)

    class Meta:
        model = Pokemon
        fields = '__all__'
