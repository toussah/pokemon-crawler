from crawler.models import Ability, Move, Pokemon, Type
from crawler.serializers import AbilitySerializer, MoveReadOnlySerializer, PokemonReadOnlySerializer, TypeSerializer
from crawler.utils import refresh_pokemons
from django.shortcuts import render
from rest_framework import generics, views


##################################################
#
# List Views
#
class TypeListAPI(generics.ListAPIView):
    """
    Lists all pokemon in a paginated manner.
    """
    queryset = Type.objects.all()
    serializer_class = TypeSerializer


class AbilityListAPI(generics.ListAPIView):
    """
    Lists all pokemon in a paginated manner.
    """
    queryset = Ability.objects.all()
    serializer_class = AbilitySerializer


class MoveListAPI(generics.ListAPIView):
    """
    Lists all pokemon in a paginated manner.
    """
    queryset = Move.objects.all()
    serializer_class = MoveReadOnlySerializer


class PokemonListAPI(generics.ListAPIView):
    """
    Lists all pokemon in a paginated manner.
    """
    queryset = Pokemon.objects.all()
    serializer_class = PokemonReadOnlySerializer


##################################################
#
# Get views
#
class TypeAPI(generics.RetrieveAPIView):
    """
    Retrieve pokemon by name.
    """
    queryset = Type.objects.all()
    serializer_class = TypeSerializer
    lookup_field = 'name'


class AbilityAPI(generics.RetrieveAPIView):
    """
    Retrieve pokemon by name.
    """
    queryset = Ability.objects.all()
    serializer_class = AbilitySerializer
    lookup_field = 'name'


class MoveAPI(generics.RetrieveAPIView):
    """
    Retrieve pokemon by name.
    """
    queryset = Move.objects.all()
    serializer_class = MoveReadOnlySerializer
    lookup_field = 'name'


class PokemonAPI(generics.RetrieveAPIView):
    """
    Retrieve pokemon by name.
    """
    queryset = Pokemon.objects.all()
    serializer_class = PokemonReadOnlySerializer
    lookup_field = 'name'


class PokedexAPI(generics.RetrieveAPIView):
    """
    Retrieve pokemon by national dex number.
    """
    queryset = Pokemon.objects.all()
    serializer_class = PokemonReadOnlySerializer
    lookup_field = 'id'


class RefreshPokemonsAPI(views.APIView):
    """
    Post request to trigger a refresh of all pokemon and related tables.
    """
    def post(self, request):
        """
        Triggers refresh
        """
        refresh_pokemons()
