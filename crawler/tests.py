from crawler.crawler import Crawler
from crawler.models import Ability, Move, Pokemon, Type
from crawler.serializers import TypeSerializer
from crawler.views import PokemonListAPI
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient, APIRequestFactory
import requests


class TestCrawler(TestCase):
    """
    Tests the different aspects of the crawler.
    """
    def test_response_format(self):
        """
        Tests that the response format of pokeapi hasn't changed.
        This allows for finding breaking changes quickly.
        """
        crawler = Crawler(Type._endpoint)
        name, url = next(crawler.list_urls())
        result = crawler.get_result(url.strip('/').split('/')[-1])
        self.assertIn('name', result, f"Error fetching {url}")

    def test_save(self):
        """
        Tests saving a result
        """
        crawler = Crawler(Type._endpoint, serializer=TypeSerializer)
        errors = crawler.save_results()
        self.assertEqual(errors, [], errors)
        self.assertGreater(Type.objects.count(), 0)

    @classmethod
    def tearDownClass(cls):
        """
        Deletes created instances.
        """
        Type.objects.all().delete()


class TestAPI(TestCase):
    """
    Tests the class-based views.
    """
    @classmethod
    def setUpClass(cls):
        """
        Sets API client and factory.
        Creates some types.
        """
        cls.client = APIClient()
        cls.factory = APIRequestFactory()
        for i in range(9):
            ability = Ability.objects.create(id=i, name=f"ability-{i}")
            type_ = Type.objects.create(id=i, name=f"type-{i}")
            move = Move.objects.create(id=i, name=f"move-{i}", type=type_)
            pokemon = Pokemon.objects.create(id=i, name=f"pokemon-{i}")
            pokemon.types.add(type_)
            pokemon.abilities.add(ability)
            pokemon.moves.add(move)

    @classmethod
    def tearDownClass(cls):
        """
        Deletes created instances of Ability, Type, Move, and Pokemon
        """
        Type.objects.all().delete()
        Move.objects.all().delete()
        Ability.objects.all().delete()
        Pokemon.objects.all().delete()

    def test_list_view(self):
        """
        Tests the List API views.
        """
        request = self.factory.get(reverse('crawler-api:pokemons'))
        response = PokemonListAPI.as_view()(request)
        self.assertEqual(response.status_code, 200)
        data = response.data
        self.assertIn('count', data, data)
        self.assertEqual(data['count'], 9, f"Expected 9 results, found {data['count']}")
        for i, result in enumerate(data['results']):
            expected_result = {'id': i, 'name': f'pokemon-{i}', 'moves': [f'move-{i}'], 'abilities': [f'ability-{i}'],
                              'types': [f'type-{i}'], 'height': None, 'weight': None}
            self.assertEqual(result, expected_result)
