from concurrent.futures import ThreadPoolExecutor
from json import JSONDecodeError
from rest_framework.exceptions import ValidationError
from urllib.parse import urljoin
import logging
import requests
TIMEOUT = 10

log = logging.getLogger(__name__)


class Crawler:
    """
    Helper class to crawl through the pokeapi API.
    """
    base_url = "https://pokeapi.co/api/v2"

    def __init__(self, endpoint, serializer=None, limit=100):
        """
        Constructor: sets the endpoint to crawl, e.g pokemon, type, move, etc. and an optional model associated to it.
        """
        self.endpoint = endpoint
        self.serializer = serializer
        self.limit = limit

    @property
    def endpoint_url(self):
        """
        Short-hand for endpoint URL.
        """
        return f"{self.base_url}/{self.endpoint}"

    def list_urls(self):
        """
        Yields all names and URLs of the endpoint by paginating through it.
        :raises: requests.exceptions.RequestException if fails to reach endpoint.
        :raises: ValueError if API result has unexpected content.
        """
        next_page = f"{self.endpoint_url}?limit={self.limit}"
        while next_page:
            log.info(f"Processing page {next_page}...")
            print(next_page)
            r = requests.get(next_page, timeout=TIMEOUT)
            try:
                response = r.json()
            except JSONDecodeError as exc:
                raise ValueError(f"Error parsing response from {r.url}: {r.status_code} - {r.content}") from exc
            for result in response['results']:
                yield result['name'], result['url']
            next_page = response.get('next')
        log.info("Last page.")

    def get_result(self, identifier):
        """
        Get results from API for specific ID or name of endpoint.
        :raises: requests.exceptions.RequestException if fails to reach endpoint.
        :raises: ValueError if API result has unexpected content.
        """
        url = f"{self.endpoint_url}/{identifier}"
        r = requests.get(url, timeout=TIMEOUT)
        try:
            return r.json()
        except JSONDecodeError as exc:
            raise ValueError(f"Error parsing response from {r.url}: {r.status_code} - {r.content}") from exc

    def list_results(self):
        """
        Crawls through all the paginated results of the endpoint and return the list of results associated.
        Uses threading for better performance on IO-bound requests.
        :raises: requests.exceptions.RequestException if fails to reach endpoint.
        :raises: ValueError if API result has unexpected content.
        """
        with ThreadPoolExecutor() as ex:
            results = ex.map(self.get_result, (url.strip('/').split('/')[-1] for _, url in self.list_urls()))
        return results

    def save_results(self):
        """
        Crawls through the paginated results of the endpoint and saves them to the database.
        Returns errors encountered.
        """
        errors = []
        if not self.serializer:
            raise RuntimeError("No model was provided to the crawler")
        model = self.serializer.Meta.model
        for result in self.list_results():
            try:
                instance = model.objects.get(pk=result['id'])
                serializer = self.serializer(instance, data=result)
            except model.DoesNotExist:
                serializer = self.serializer(data=result)
            if serializer.is_valid():
                serializer.save()
            else:
                errors.append((result['name'], serializer.errors))
        return errors
