# Pokemon Crawler

Project started from https://docs.docker.com/compose/django/

Some useful commands:

* `docker-compose up`
* `docker-compose exec web bash`
* `docker-compose exec web python -m pip install -r requirements.txt`

Endpoints:
* http://localhost:8000/crawler/api/v1/pokemons
* http://localhost:8000/crawler/api/v1/types
* http://localhost:8000/crawler/api/v1/moves
* http://localhost:8000/crawler/api/v1/abilities

* http://localhost:8000/crawler/api/v1/pokemon/bulbasaur
* http://localhost:8000/crawler/api/v1/pokedex/1
* http://localhost:8000/crawler/api/v1/refresh (POST)
